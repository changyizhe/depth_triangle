%convert a ply file to .bin file for openGL processing

list=[1,2,3,4,5,6,9,14,15,16,17,18,19,21,22,23,34,38,39,40,41];
%list=[1];
for i=1:size(list,2)
    if(list(i)<10)
        face(i)=ply_to_vertex(strcat('scene_in/tola/tola00',num2str(list(i)),'_l3_surf_11_trim_8.ply'),strcat('scene_out/tola/tola00',num2str(list(i)),'_l3_surf_11_trim_8.bin'));
    else
        face(i)=ply_to_vertex(strcat('scene_in/tola/tola0',num2str(list(i)),'_l3_surf_11_trim_8.ply'),strcat('scene_out/tola/tola0',num2str(list(i)),'_l3_surf_11_trim_8.bin'));
    end
end
%face2=ply_to_vertex('scene_in/tola001_l3_surf_11.ply','scene_out/tola001_l3_surf_11.bin');
%face3=ply_to_vertex('scene_in/tola001_l3_surf_11.ply','scene_out/tola001_l3_surf_11.bin');
