%read ply file to vertex file, directly give file name

function [numface]=ply_to_vertex(fileNameIn,fileNameOut)
    try
        [Element,var]=plyread(fileNameIn);
    catch ME
        disp(ME.message);
        numface=0;
        return;
    end
    %Outfile=strcat('Poisson/vertex_img',int2str(imageID),'.bin');
    Outfile=fileNameOut;

    disp(sprintf('Done read file %s',fileNameIn));
    verts=[Element.vertex.x Element.vertex.y Element.vertex.z];
    faces=cell2mat(Element.face.vertex_indices)+1;
    
    numvert=size(verts,1);
    numface=size(faces,1);
    
    temp=verts(faces',:)';
    outvert=reshape(temp,9,[]);
    
    fid=fopen(Outfile,'w');
    %fprintf(fid,'%d\n%d\n',numvert,numface);
    fwrite(fid,[numvert,numface],'int');
    fwrite(fid,outvert,'float');
    %for i=1:numface
    %    x1=Element.vertex.x(Element.face.vertex_indices{i,1}(1)+1);
    %    y1=Element.vertex.y(Element.face.vertex_indices{i,1}(1)+1);
    %    z1=Element.vertex.z(Element.face.vertex_indices{i,1}(1)+1);
    %    x2=Element.vertex.x(Element.face.vertex_indices{i,1}(2)+1);
    %    y2=Element.vertex.y(Element.face.vertex_indices{i,1}(2)+1);
    %    z2=Element.vertex.z(Element.face.vertex_indices{i,1}(2)+1);
    %    x3=Element.vertex.x(Element.face.vertex_indices{i,1}(3)+1);
    %    y3=Element.vertex.y(Element.face.vertex_indices{i,1}(3)+1);
    %    z3=Element.vertex.z(Element.face.vertex_indices{i,1}(3)+1);
        %fprintf(fid,'%f %f %f %f %f %f %f %f %f\n',x1,y1,z1,x2,y2,z2,x3,y3,z3);
    %    fwrite(fid,[x1 y1 z1 x2 y2 z2 x3 y3 z3],'float');
    %end
    fclose(fid);
    disp(sprintf('Done write file %s, there are %i faces',Outfile,numface));

end
