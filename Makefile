CPP  = g++
CC   = gcc

LIBS =  -L"/usr/lib" -lglut -lGLU -lGL
INCS =  -I"/usr/include" 

CXXFLAGS = $(CXXINCS) -Wall   -O2

viewer_YC : main_YC.o
	$(CPP) $^ -o $@ $(LIBS)

main_YC.o: main_YC.cpp
	$(CPP) -c main_YC.cpp -o main_YC.o $(CXXFLAGS) 