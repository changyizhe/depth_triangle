// ------------------- 03/23/2011 ------------------- //
// ------------- Modified by Xiaoyan Hu ------------- //

// ------------------- 03/12/2017 ------------------- //
// ------------- Modified by changyizhe@hotmail.com-- //
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <string>
#include <cmath>


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define PI 3.1415926;

using namespace std;

float NumTriangles;
float* data = NULL;
float* depth = NULL;
float *z_eye=NULL;

int width;
int height;

int imgNo;
std::string CamName[12],ImgName;
// float scale;

// width and height determines the size of the GLUT window and
// the size of the generated depth map
// const int width  = 1536;
// const int height = 1024;

double ModelViewMatrix[16];

float zNear;
float zFar;

// // Render the scene multiple times to get more accurate
// // depth buffer
// const float zNear = 4.0;
// const float zFar  = 20.0;

const int MaxTotalView = 20;
int TotalView;
int ViewIdx = 0;

float R[MaxTotalView][3][3], T[MaxTotalView][3];
float K[MaxTotalView][3][3];
GLfloat projection_matrix[16];

// float det3x3(float a1, float a2, float a3, float a4, float a5,
// 	     float a6, float a7, float a8, float a9)
// {
//   return a1*a5*a9 + a4*a8*a3 + a7*a2*a6
//     - a7*a5*a3 - a4*a2*a9 - a1*a8*a6 ;
// }


// void inv_3x3(float a[3][3], float inv[3][3])
// {
//   float det = det3x3(a[0][0], a[0][1], a[0][2], a[1][0], a[1][1],
// 		     a[1][2], a[2][0], a[2][1], a[2][2]);
	
//   inv[0][0] =   (a[1][1]*a[2][2] - a[2][1]*a[1][2])/det;
//   inv[1][0] = - (a[1][0]*a[2][2] - a[2][0]*a[1][2])/det;
//   inv[2][0] =   (a[1][0]*a[2][1] - a[2][0]*a[1][1])/det;
	
//   inv[0][1] = - (a[0][1]*a[2][2] - a[2][1]*a[0][2])/det;
//   inv[1][1] =   (a[0][0]*a[2][2] - a[2][0]*a[0][2])/det;
//   inv[2][1] = - (a[0][0]*a[2][1] - a[2][0]*a[0][1])/det;
	
//   inv[0][2] =   (a[0][1]*a[1][2] - a[1][1]*a[0][2])/det;
//   inv[1][2] = - (a[0][0]*a[1][2] - a[1][0]*a[0][2])/det;
//   inv[2][2] =   (a[0][0]*a[1][1] - a[1][0]*a[0][1])/det;
// }


// void MatMul3_3x3_1(float a[3][3], float b[3], float c[3])
// {
//   c[0] = a[0][0]*b[0] + a[0][1]*b[1] + a[0][2]*b[2];
//   c[1] = a[1][0]*b[0] + a[1][1]*b[1] + a[1][2]*b[2];
//   c[2] = a[2][0]*b[0] + a[2][1]*b[1] + a[2][2]*b[2];
// }


// void RefPt(float pt[3])
// {
//   float K_[3][3], InvK[3][3];
//   K_[0][0] = K[ViewIdx][0][0]; 
//   K_[0][1] = K[ViewIdx][0][1]; 
//   K_[0][2] = K[ViewIdx][0][2];
  
//   K_[1][0] = K[ViewIdx][1][0]; 
//   K_[1][1] = K[ViewIdx][1][1]; 
//   K_[1][2] = K[ViewIdx][1][2];

//   K_[2][0] = K[ViewIdx][2][0]; 
//   K_[2][1] = K[ViewIdx][2][1]; 
//   K_[2][2] = K[ViewIdx][2][2];

//   inv_3x3(K_, InvK);
 
//   float pt_c[3];
//   pt_c[0] = K_[0][2]*zNear;
//   pt_c[1] = K_[1][2]*zNear;
//   pt_c[2] = zNear;

//   cout << "pt_c: " 
//        << pt_c[0] << " " 
//        << pt_c[1] << " "
//        << pt_c[2] << endl;

//   cout << InvK[0][0] << " " << InvK[0][1] << " " << InvK[0][2] << endl
//        << InvK[1][0] << " " << InvK[1][1] << " " << InvK[1][2] << endl
//        << InvK[2][0] << " " << InvK[2][1] << " " << InvK[2][2] << endl
//        << endl;

//   MatMul3_3x3_1(InvK, pt_c, pt);
// }


void ReadConfig(string ConfigName)
{
  std::ifstream ifm;

  float scale;
  // int TotalImgNum;
	
  // Open the configuration file
  ifm.open(ConfigName.c_str());
	
  // Exit if the file cannot be openned
  if(!ifm.good())
    {
      std::cerr<<"Cannot open config.ini !!!"<<std::endl;
      exit(1);
    }
		
  // Read number of images, sweeping distance, image name and scale
  ifm >> TotalView >> scale;
	
  // Start reading projective matrices
  for(int i=0; i<TotalView; i++)
    {
      // Firstly read the image name
      
      ifm >> CamName[i];
		  
      ifm >> K[i][0][0] >> K[i][0][1] >> K[i][0][2] 
	  >> K[i][1][0] >> K[i][1][1] >> K[i][1][2]
	  >> K[i][2][0] >> K[i][2][1] >> K[i][2][2];      

      //YC Note: Not so sure what this "scale" for, always set it to 1!
      /*K[i][0][0] *= scale;
      K[i][0][1] *= scale; 
      K[i][0][2] *= scale;
      K[i][1][0] *= scale;
      K[i][1][1] *= scale;
      K[i][1][2] *= scale;*/

      /*cout << K[i][0][0] << " " << K[i][0][1] << " " << K[i][0][2] << endl
	   << K[i][1][0] << " " << K[i][1][1] << " " << K[i][1][2] << endl
	   << K[i][2][0] << " " << K[i][2][1] << " " << K[i][2][2] << endl
	   << endl;*/
			
      ifm >>  R[i][0][0] >> R[i][0][1] >> R[i][0][2]
	  >>  R[i][1][0] >> R[i][1][1] >> R[i][1][2]
	  >>  R[i][2][0] >> R[i][2][1] >> R[i][2][2];
			
      ifm >>  T[i][0] >> T[i][1] >> T[i][2];
      
    }

  ifm.close();
}

void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glShadeModel(GL_FLAT);
  glEnable(GL_DEPTH_TEST);

  //glDepthRange(-1,1);
  //glEnable(GL_LIGHTING);
  //   glEnable(GL_LIGHT0);
}


// P matrix for image 5 is
// [2246.17 -2208.64 -62.134 24477.4
//  -316.161 -1091.08 2713.64 -8334.18 
//  -0.269944 -0.961723 -0.0471142 -7.01217]

void display()
{
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glColor3f(1.0, 1.0, 1.0);
  glLoadIdentity();

  //YC Note: set the viewport, no real use in this application
  //glViewport(0,0,width,height);

  // Configuration for fountain data
  // Model view matrix for view 5
  // Model view matrix is [R|T], note that the sign is reversed for the third column (third row actually)
  // double ModelViewMatrix[] = { 0.96274   ,  -0.016055 ,  0.26994  ,  0,
  // 			       -0.2704    ,  -0.044428 ,  0.96172  ,  0,
  // 			       0.0034471 ,   0.99888  ,  0.047114 ,  0,
  // 			       12.7346   ,  -0.46099  ,  7.0122   ,  1   };

  /*if(ViewIdx < TotalView)
    {
      // double ModelViewMatrix[16] = {0};
      memset(ModelViewMatrix, 0, sizeof(double)*16);
      ModelViewMatrix[15] = 1.0;

      for(int i=0; i<3; i++)
      	{
      	  for(int j=0; j<3; j++)
      	    {
      	      ModelViewMatrix[i*4 + j] = R[ViewIdx][j][i];
	      
      	      if(j==2)
      		ModelViewMatrix[i*4 + j] = -ModelViewMatrix[i*4 + j];
      	    }
      	}
      
      ModelViewMatrix[12] =  T[ViewIdx][0];
      ModelViewMatrix[13] =  T[ViewIdx][1];
      ModelViewMatrix[14] = -T[ViewIdx][2];

      // if(ViewIdx == 5)
      // 	cout << ModelViewMatrix[0]  << " " 
      // 	     << ModelViewMatrix[1]  << " "
      // 	     << ModelViewMatrix[2]  << " " 
      // 	     << ModelViewMatrix[3]  << endl
      // 	     << ModelViewMatrix[4]  << " "
      // 	     << ModelViewMatrix[5]  << " " 
      // 	     << ModelViewMatrix[6]  << " "
      // 	     << ModelViewMatrix[7]  << endl 
      // 	     << ModelViewMatrix[8]  << " "
      // 	     << ModelViewMatrix[9]  << " "
      // 	     << ModelViewMatrix[10] << " "
      // 	     << ModelViewMatrix[11] << endl
      // 	     << ModelViewMatrix[12] << " "
      // 	     << ModelViewMatrix[13] << " "
      // 	     << ModelViewMatrix[14] << " "
      // 	     << ModelViewMatrix[15] << endl; 

      // // R matrix 
      // ModelViewMatrix[0]  = R[ViewIdx][0][0];
      // ModelViewMatrix[4]  = R[ViewIdx][0][1];
      // ModelViewMatrix[8]  = R[ViewIdx][0][2];

      // ModelViewMatrix[1]  = R[ViewIdx][1][0];
      // ModelViewMatrix[5]  = R[ViewIdx][1][1];
      // ModelViewMatrix[9]  = R[ViewIdx][1][2];

      // ModelViewMatrix[2]  = -R[ViewIdx][2][0];
      // ModelViewMatrix[6]  = -R[ViewIdx][2][1];
      // ModelViewMatrix[10] = -R[ViewIdx][2][2];

      // // T matrix 
      // ModelViewMatrix[12] = T[ViewIdx][0];
      // ModelViewMatrix[13] = T[ViewIdx][1];
      // ModelViewMatrix[14] = -T[ViewIdx][2];

      // ModelViewMatrix[3]  = 0;
      // ModelViewMatrix[7]  = 0;
      // ModelViewMatrix[11] = 0;
      // ModelViewMatrix[15] = 1;
    }*/


  //glMultMatrixd(ModelViewMatrix);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  // gluPerspective(40.6549, 1.5, zNear, zFar);      
  
  //YC Note: the GL_PROJECTION using gluPerspective should be the same result as using the glFrustum
  //float fovy = 2*180*atan((float)height/(K[ViewIdx][1][1]*2))/PI;
  //cout << "fovy: " << fovy << endl;
  //float aspect = (float)width/(float)height*(K[ViewIdx][1][1]/K[ViewIdx][0][0]);
  //cout << "aspect: " << aspect << endl;
  //gluPerspective(fovy, aspect, zNear, zFar);

  /*glFrustum(-zNear*(width/2)/K[ViewIdx][0][0],zNear*(width/2)/K[ViewIdx][0][0]
    ,-zNear*(height/2)/K[ViewIdx][1][1],zNear*(height/2)/K[ViewIdx][1][1]
    ,zNear,zFar);*/

  //YC Note: manually load projection matrix
  //see http://www.songho.ca/opengl/gl_projectionmatrix.html matrix after eq(3)
  float l,r,t,b;
  l=zNear*(.0-K[ViewIdx][2][0])/K[ViewIdx][0][0];
  r=zNear*((float)width-K[ViewIdx][2][0])/K[ViewIdx][0][0];

  b=zNear*(.0-K[ViewIdx][2][1])/K[ViewIdx][1][1];
  t=zNear*((float)height-K[ViewIdx][2][1])/K[ViewIdx][1][1];

  for(int i(0);i<16;i++){
    projection_matrix[i]=.0;
  }
  //column first;
  projection_matrix[0]=2.0*zNear/(r-l);
  projection_matrix[5]=2.0*zNear/(t-b);
  projection_matrix[8]=(r+l)/(r-l);
  projection_matrix[9]=(t+b)/(t-b);
  projection_matrix[10]=-(zNear+zFar)/(zFar-zNear);
  projection_matrix[11]=-1.0;
  projection_matrix[14]=-2.0*zNear*zFar/(zFar-zNear);
  
  glMultMatrixf(projection_matrix);

  //YC Note: load the projection_matrix for saving the true distance
  //glGetFloatv(GL_PROJECTION_MATRIX,projection_matrix);
  /*cout<<"Projection matrix"<<endl;
  for(int k(0);k<4;k++){
      cout<<projection_matrix[k+0]<<" "<<projection_matrix[k+4]<<" "<<projection_matrix[k+8]<<" "<<projection_matrix[k+12]<<endl;
  }*/

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();  
  if(ViewIdx < TotalView){//Set camera's position
    //gluLookAt(1000,0,600,999,0,600,0,0,1);
    
    gluLookAt(T[ViewIdx][0],T[ViewIdx][1],T[ViewIdx][2],
      T[ViewIdx][0]+R[ViewIdx][0][2],T[ViewIdx][1]+R[ViewIdx][1][2],T[ViewIdx][2]+R[ViewIdx][2][2],
      -R[ViewIdx][0][1],-R[ViewIdx][1][1],-R[ViewIdx][2][1]);
    
    //glMultMatrixd(ModelViewMatrix);
    /*GLfloat model[16]; 
    glGetFloatv(GL_MODELVIEW_MATRIX, model); 
    cout<<"model view matrix"<<endl;
    for(int k(0);k<4;k++){
      cout<<model[k+0]<<" "<<model[k+4]<<" "<<model[k+8]<<" "<<model[k+12]<<endl;
    }*/
  }
  else{//Stay the screen on the 10th camera after all commands executed, delete exit(0) to display this;
    gluLookAt(T[9][0],T[9][1],T[9][2],
      T[9][0]+R[9][0][2],T[9][1]+R[9][1][2],T[9][2]+R[9][2][2],
      R[9][0][1],R[9][1][1],R[9][2][1]);
  }
  

  // float ref_pt[3];
  // RefPt(ref_pt);
  // cout << "ref_pt: " 
  //      << ref_pt[0] << " " 
  //      << ref_pt[1] << " "
  //      << ref_pt[2] << endl;

  // gluLookAt(0, 0, 0, ref_pt[0], ref_pt[1], ref_pt[2], 0, 1, 0);
	
  int num = 3*3*NumTriangles;

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  glBegin(GL_TRIANGLES);
  for(int i=0; i<num; i+=3){
    glVertex3f(data[i], data[i+1], data[i+2]);
    //cout<<"data: "<<data[i]<<" "<<data[i+1]<<" "<<data[i+2]<<endl;
  }
  glEnd();
	
	//glGetFloatv(GL_PROJECTION_MATRIX,projection_matrix);

  if( ViewIdx < TotalView )
    {
      //glClearDepth(0.44);
      glReadPixels(0, 0, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, depth);
      

      for(int i(0);i<width*height;i++){
        z_eye[i] = projection_matrix[14]/(depth[i] * 2.0 - 1.0 + projection_matrix[10]);
        //z_eye[i] = depth[i];
      }

      //imgHorizontalFlip(z_eye,width,height);

      //cout<<"depth"<<depth[0]<<" "<<z_eye[0]<<endl;
      //cout<<"matrix:"<<projection_matrix[14]<<" , "<<projection_matrix[10]<<endl;
      //cout<<"depth"<<depth[240400]<<endl;
      stringstream ssm;
      ssm << ImgName << "_"<<CamName[ViewIdx] << ".depth"; 
      
      ofstream ofm;

      ofm.open(ssm.str().c_str(), ios::trunc | ios::binary);
      ofm.write((char*)z_eye, width*height*sizeof(float));
      ofm.close();

      if(ViewIdx < TotalView){
        cout << "View " << ViewIdx << " done!" << endl; 
        ViewIdx++;
      }

      if(ViewIdx == TotalView){
        cout << "Done!" << endl;
        exit(0);
      }
      //else ViewIdx++;
    }
	
  glutSwapBuffers();
}

void reshape(int w, int h)
{
  //  Translate viewport according to principal point
  glViewport((int)(K[ViewIdx][0][2]-(float)width/2+0.5), 
	     (int)(K[ViewIdx][1][2]-(float)height/2+0.5),
	     (GLsizei)w, (GLsizei)h);
  //	glMatrixMode(GL_PROJECTION);
  //	glLoadIdentity();
  //
  //	// Parameters for gluPerspective are fovy, aspect, near, far
  //	// fovy is calculated from K, atan(py/f)
  //	// aspect = ImWidth/ImHeight
  //	// near and far are determined by bounding box
  //	// This is the configuration for Fountain data
  //	gluPerspective(40.6549, 1.5, 4.4513, 10.4467);
	
  //glMatrixMode(GL_MODELVIEW);
  //glLoadIdentity();
}

void LoadDataFile(string InputName, float& NumTriangles, float*& loaddata)
{
  ifstream ifm;
  ifm.open(InputName.c_str(), ios::binary);

  if(!ifm.good()){
    cerr<<"Cannot open"<<InputName<<"!"<<endl;
    exit(1);
  }
	
  int nv, nt;

  ifm.read((char*)&nv, sizeof(int));
  ifm.read((char*)&nt, sizeof(int));

  loaddata = new float[(int)nt*3*3];
  NumTriangles = nt;

  cout << InputName<<" NumTriangles= " << NumTriangles << "\n";

  ifm.read((char*)loaddata, sizeof(float)*nt*3*3);

  ifm.close();
}

void ReleaseData(float*& loaddata)
{
  delete[] loaddata;
  if(depth!=NULL)
    delete[] depth;
  if(z_eye!=NULL)
    delete[] z_eye;
  loaddata = NULL;
}


void idle()
{
  glutPostRedisplay();
}

void clean()
{
  ReleaseData(data);
}


int main(int argc, char** argv)
{
  int ArgCount = 1;

  string ConfigName = argv[ArgCount++];
  string InputName  = argv[ArgCount++];
  ImgName=argv[ArgCount++];

  width  = atoi(argv[ArgCount++]);
  height = atoi(argv[ArgCount++]);
  zNear  = atof(argv[ArgCount++]);
  zFar   = atof(argv[ArgCount++]);
  
  //imgNo  = atoi(argv[ArgCount++]);
  // TotalView = atoi(argv[ArgCount++]);

  depth = new float[width*height];
  z_eye=new float[width*height];
  memset(depth, 0, sizeof(float)*width*height);
  LoadDataFile(InputName, NumTriangles, data);
  ReadConfig(ConfigName);

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
  glutInitWindowSize(width, height);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("TriangleViewer");
  init();
  
  atexit(clean);
 
  glutDisplayFunc(display);
  //glutReshapeFunc(reshape);
  glutIdleFunc(idle);
  glutMainLoop();
  
  return 0;
}



//void imgHorizontalFlip(float imgIn[],int imgWidth,int imgHeight);
// -------------------------------------------------- //
// -------------------------------------------------- //


void imgHorizontalFlip(float imgIn[],int imgWidth,int imgHeight){
  float *temp(NULL);
  temp=new float[imgWidth*imgHeight];

  for(int i(0);i<imgHeight;i++){
    for(int j(0);j<imgWidth;j++){
      temp[i*imgHeight+j]=imgIn[i*imgHeight+(j-1)];
    }
  }
  for(int i(0);i<imgHeight;i++){
    for(int j(0);j<imgWidth;j++){
      imgIn[i*imgHeight+j]=temp[i*imgHeight+j];
    }
  }
  delete[] temp;
}