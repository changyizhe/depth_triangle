zNear =  13.0;
zFar  = 55.0;
width = 1024;
height = 768;

file = dir('*.dat');

NumOfFile = size(file, 1);

for i = 1:NumOfFile
    
    fid = fopen(file(i).name);
    depth = fread(fid, 'float32');
    fclose(fid);
    
    coefA = 2 * zFar * zNear;
    coefB =     zFar + zNear;
    coefC =     zFar - zNear;
    
    depth = coefA./(coefB - coefC.*(2.*depth-1));
   
    depth = reshape(depth, [width height]);
    
    maxZ = 55.0;
    minZ = 13.0;
    
    disparity=uint8(zeros(width, height));
    for i2=1:height
        for i1=1:width
            disparity(i1,i2) = (1 - depth(i1,i2)/maxZ)/((depth(i1,i2)/255)*(1/minZ-1/maxZ));
        end
    end
    
    c = sscanf(file(i).name, '%*4c%d%*4c');
    name = sprintf('%s%d%s', 'depth-cam', c, '-f0029.png');
    if (c==19)
        imwrite(disparity', name, 'PNG');
    end
end
