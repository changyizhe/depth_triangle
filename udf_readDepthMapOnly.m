%%-----------------------------------------------------------------------------------
% Program.........: udf_readDepthMapOnly
% Description.....: reads depth map from specified dir
% Returns.........: depth map
% Versions........: 2015-06-04/A. Spyropoulos: Initial 
% -----------------------------------------------------------------------------------

function depthMap = udf_readDepthMapOnly (fullFileName, numRows, numColumns)

% -- Load the file --    
fid = fopen(fullFileName, 'r');

if fid > 0
    %depthMap(1:600,1:800)=3;
    data1 = fread(fid, 'float32');
    fclose(fid);
    %for i=1:numColumns
    %    for j=1:numRows
    %        depthMap(i,j)=data1((i-1)*numRows+j);
    %    end
    %end
    
    depthMap = reshape(data1, [numColumns numRows])';
    %depthMap=flip(depthMap,1);
else
    depthMap = [];
end

return
