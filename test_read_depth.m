%bash file to load depthmap, depthmap2 for visulization only

%zFar=1100;
%zNear=100;
clear all
close all
depthmap=udf_readDepthMapOnly('depth/camp005_l3_surf_11_trim_8_cam10.depth',600,800);
imshow(depthmap);
depthmap2=udf_readDepthMapOnly('depth/camp005_l3_surf_11_trim_8_cam10.depth',600,800)/1800;
%depthmap2=flip(depthmap2,1);
figure, imshow(depthmap2);
%world_depthmap=depthmap*(zFar-zNear)+zNear;
%depth_tsinghua=udf_readDepthMapOnly('View13.dat',1043,768);
%imshow(depth_tsinghua);