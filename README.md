# depth_triangle for Linux

This repo includes .m file and opengl file to convert a mesh .ply file into a depth map

To use this repo, you need:

1. camera information .ini file (see PoissonConfig_modified.ini as example)
2. .ply file (see scene_in/furu001_l3_surf_11.ply as example)

To convert,

1. in matlab, run convert_ply.m. This should yield a .bin file (at scene_out)
2. in terminal `bash convert.sh`. You may want to convert a batch by using `bash convert_batch.sh`. If something is wrong, try `LD_PRELOAD=/lib/x86_64-linux-gnu/libpthread.so.0 bash convert.sh`
3. to visualize the yielded depth file (in depth folder by default),  in matlab run visualize_depth.m

To compile the c++ file, simply in terminal `make`


Note: DepthTranslator.m is not used as the c++ file already yield depth file in world coordiante.